﻿using AutoMapper;
using RecipesMVC.Entities;
using RecipesMVC.ModelsDto;
using RecipesProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipesMVC.Helpers
{
    public class RecipeMappingProfile : Profile
    {
        public RecipeMappingProfile()
        {
            CreateMap<Recipe, RecipeDto>()
                .ReverseMap();

            CreateMap<Category, CategoryDto>().ReverseMap();

            CreateMap<User, UserDto>().ReverseMap();

            CreateMap<Tag, TagDto>().ReverseMap();

            CreateMap<Role, RoleDto>().ReverseMap();

            CreateMap<RegisterUserDto, User>();
        }
    }
}
