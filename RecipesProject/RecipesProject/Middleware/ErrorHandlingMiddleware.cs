﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using RecipesMVC.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipesMVC.Middleware
{
    public class ErrorHandlingMiddleware : IMiddleware
    {
        private readonly ILogger<ErrorHandlingMiddleware> _logger;

        public ErrorHandlingMiddleware(ILogger<ErrorHandlingMiddleware> logger)
        {
            _logger = logger;
        }
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next.Invoke(context);
            }
            catch (ForbiddenException forbiddentException)
            {
                await Response(context, forbiddentException, StatusCodes.Status403Forbidden);
            }
            catch (BadRequestException badrequestEx)
            {
                await Response(context, badrequestEx, StatusCodes.Status400BadRequest);
            }
            catch (NotFoundException notFoundEx)
            {
                await Response(context, notFoundEx, StatusCodes.Status404NotFound);
            }
            catch (Exception ex)
            {
                await Response(context, ex, StatusCodes.Status500InternalServerError);
            }
        }
        private async Task Response(HttpContext context, Exception ex, int code)
        {
            _logger.LogError(ex, ex.Message);

            context.Response.StatusCode = code;
            await context.Response.WriteAsync(ex.Message);
        }
    }
}
