﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RecipesMVC.ModelsDto;

namespace RecipesMVC.Entities
{
    public class DbContextRecipe : DbContext
    {

        public DbContextRecipe(DbContextOptions<DbContextRecipe> options) : base(options)
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<User> Users { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Recipe>()
                .Property(r => r.Title)
                .IsRequired()
                .HasMaxLength(50);

            modelBuilder.Entity<Recipe>()
                .Property(r => r.CreatedByUserId)
                .IsRequired();

            modelBuilder.Entity<Recipe>()
                .Property(r => r.CategoryId)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(a => a.Name)
                .IsRequired();

            modelBuilder.Entity<Role>()
                .Property(p => p.Name)
                .IsRequired();

            modelBuilder.Entity<Tag>()
                .Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(15);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // optionsBuilder.UseSqlServer(_connectionString);
        }
        public DbSet<RecipesMVC.ModelsDto.UserDto> UserDto { get; set; }
      
    }
}
