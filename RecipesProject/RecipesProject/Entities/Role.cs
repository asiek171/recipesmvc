﻿using RecipesMVC.ModelsDto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipesMVC.Entities
{
    public class Role : BaseClass
    {
        public string Name { get; set; }
        public virtual List<User> Users { get; set; }

    }
}
