﻿using RecipesMVC.Helpers.Enums;
using RecipesMVC.ModelsDto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipesMVC.Entities
{
    public class Recipe : BaseClass
    {
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        public Difficulty Difficulty { get; set; }
        public string MakingTime { get; set; }
        public int Portions { get; set; }
        public string Description { get; set; }
        public string Ingrediens { get; set; }
        [Required]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
        [Required]
        public int CreatedByUserId { get; set; }
        public virtual User CreatedByUser { get; set; }

        public virtual List<Tag> Tags { get; set; }

    }
}
