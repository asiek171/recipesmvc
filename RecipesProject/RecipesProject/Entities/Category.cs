﻿using RecipesMVC.ModelsDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipesMVC.Entities
{
    public class Category : BaseClass
    {
        public string Name { get; set; }
        public virtual List<Recipe> Recipes { get; set; }

    }
}
