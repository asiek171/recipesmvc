﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RecipesMVC.Entities;
using RecipesMVC.ModelsDto;
using RecipesMVC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipesMVC.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _service;

        public CategoryController(ICategoryService service)
        {
            _service = service;
        }
        [AllowAnonymous]
        [HttpGet]
        // GET: CategoryController
        public ActionResult Index()
        {
            var categories = _service.GetAll();
            return View(categories);
        }
        
        [AllowAnonymous]
        [HttpGet]
        // GET: CategoryController/Details/5
        public ActionResult Details(int id)
        {
            var category = _service.GetById(id);
            return View(category);
        }

        // GET: CategoryController/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: CategoryController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([FromForm] CategoryDto model)
        {

            _service.Create(model.Name);
            return RedirectToAction(nameof(Index));

        }

        // GET: CategoryController/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var category = _service.GetById(id);
            return View(category);
        }

        // POST: CategoryController/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, [FromForm] CategoryDto model)
        {

            _service.Update(id, model);
            return RedirectToAction(nameof(Index));
            //  return RedirectToAction(nameof(Edit),new { id = id });

        }

        // GET: CategoryController/Delete/5
        [HttpGet]
        public ActionResult Delete(int id)
        {

            var category = _service.GetById(id);
            return View(category);
        }

        // POST: CategoryController/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, [FromForm] CategoryDto category)
        {
            _service.Delete(id);
            return RedirectToAction(nameof(Index));

        }
    }
}
