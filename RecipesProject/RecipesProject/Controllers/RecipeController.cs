﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RecipesMVC.Entities;
using RecipesMVC.ModelsDto;
using RecipesMVC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace RecipesProject.Controllers
{
    [Authorize]
    public class RecipeController : Controller
    {
        private readonly IRecipeService _service;

        public RecipeController(IRecipeService service)
        {
            _service = service;
        }

        [AllowAnonymous]
        [HttpGet]
        // GET: RecipeController
        public ActionResult Index()
        {
            var recipes = _service.GetAll();
            return View(recipes);
        }

        [AllowAnonymous]
        [HttpGet]
        // GET: RecipeController/Details/5
        public ActionResult Details(int id)
        {
            var recipe = _service.GetById(id);
            return View(recipe);
        }
        // GET: RecipeController/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: RecipeController/Create
        [HttpPost]
        public ActionResult Create([FromForm] RecipeDto model)
        {

            _service.Create(model);
            return RedirectToAction(nameof(Index));

        }

        // GET: RecipeController/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var category = _service.GetById(id);
            return View(category);
        }

        // POST: RecipeController/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, [FromForm] RecipeDto model)
        {

            _service.Update(id, model);
            return RedirectToAction(nameof(Index));
            //  return RedirectToAction(nameof(Edit),new { id = id });

        }

        // GET: RecipeController/Delete/5
        [HttpGet]
        public ActionResult Delete(int id)
        {

            var category = _service.GetById(id);
            return View(category);
        }

        // POST: RecipeController/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, [FromForm] CategoryDto category)
        {
            _service.Delete(id);
            return RedirectToAction(nameof(Index));

        }
    }
}
