﻿using Microsoft.AspNetCore.Authorization;
using RecipesMVC.Entities;
using RecipesMVC.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RecipesMVC.Authorization
{
    public class ResourceOperationRequirementHandler : AuthorizationHandler<ResourceOperationRequirement, Recipe>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, ResourceOperationRequirement requirement, Recipe recipe)
        {
            if (requirement.ResourceOperation == ResourceOperation.Read ||
                requirement.ResourceOperation == ResourceOperation.Create)
            {
                context.Succeed(requirement);
            }

            var userId = context.User.FindFirst(u => u.Type == ClaimTypes.NameIdentifier).Value;
            bool isAdmin = ClaimTypes.Role.Equals("Admin");

            if (recipe.CreatedByUserId == int.Parse(userId) || isAdmin)
            {
                context.Succeed(requirement);
            }
            return Task.CompletedTask;
        }
    }
}
