﻿using FluentValidation;
using RecipesMVC.Entities;
using System.Linq;

namespace RecipesMVC.ModelsDto.Validators
{
    public class RegisterUserDtoValidator : AbstractValidator<RegisterUserDto>
    {
        public RegisterUserDtoValidator(DbContextRecipe dbContext)
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .EmailAddress();

            RuleFor(x => x.Name).NotEmpty();

            RuleFor(x => x.Password)
                .NotEmpty()
                .MinimumLength(3);
            
            RuleFor(x => x.ConfirmPassword).Equal(e => e.Password);

            RuleFor(x => x.Email)
                .Custom((value, context) =>
                {
                    bool emailInUse = dbContext.Users.Any(user => user.Email == value);
                    if (emailInUse)
                    {
                        context.AddFailure("Email", "Email is already in use");
                    }
                });

        }

    }
}
