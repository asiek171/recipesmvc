﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipesMVC.ModelsDto
{
    public class TagDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<RecipeDto> Recipes { get; set; }
    }
}
