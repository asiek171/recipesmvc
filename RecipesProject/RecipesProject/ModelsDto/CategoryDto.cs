﻿using RecipesMVC.ModelsDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipesMVC.ModelsDto
{
    public class CategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<RecipeDto> Recipes { get; set; }
    }
}
