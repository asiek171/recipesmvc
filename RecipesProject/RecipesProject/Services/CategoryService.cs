﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RecipesMVC.Entities;
using RecipesMVC.Exceptions;
using RecipesMVC.ModelsDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipesMVC.Services
{
    public interface ICategoryService
    {
        int Create(string categoryName);
        void Delete(int id);
        List<CategoryDto> GetAll();
        CategoryDto GetById(int id);
        void Update(int id, CategoryDto model);

    }
    public class CategoryService : ICategoryService
    {
        private readonly DbContextRecipe _context;
        private readonly IMapper _mapper;
        private readonly ILogger<CategoryService> _logger;

        public CategoryService(DbContextRecipe context, IMapper mapper, ILogger<CategoryService> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public int Create(string categoryName)
        {
            var category = new Category()
            {
                Name = categoryName
            };

            _context.Add(category);
            _context.SaveChanges();

            _logger.LogInformation($"CREATE category action with id:{category.Id}");

            return category.Id;
        }

        public void Delete(int id)
        {
            var category = _context.Categories.FirstOrDefault(r => r.Id == id);

            if (category is null)
                throw new NotFoundException("Category not found");

            _context.Remove<Category>(category);
            var result = _context.SaveChanges();

            _logger.LogWarning($"DELETE category action for id:{category.Id}");
        }


        public CategoryDto GetById(int id)
        {
            var category = _context
                .Categories
                .Include(a => a.Recipes)
                .FirstOrDefault(r => r.Id == id);

            if (category is null)
                throw new NotFoundException("Category not found");

            var result = _mapper.Map<CategoryDto>(category);

            return result;
        }

        public List<CategoryDto> GetAll()
        {
            var categories = _context.Categories.ToList();

            if (categories is null)
                throw new NotFoundException("Categories not found");

            var result = _mapper.Map<List<CategoryDto>>(categories);

            return result;
        }

        public void Update(int id, CategoryDto model)
        {
            var category = _context.Categories.FirstOrDefault(r => r.Id == id);

            if (category is null)
                throw new NotFoundException("Category not found");

            category = _mapper.Map<Category>(model);

            _context.Update(category);
            _context.SaveChanges();

            _logger.LogInformation($"UPDATE category action for id:{category.Id}");
        }
    }
}
