﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using RecipesMVC.Entities;
using RecipesMVC.Exceptions;
using RecipesMVC.ModelsDto;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RecipesMVC.Services
{
    public interface IAccountService
    {
        List<UserDto> GetAll();
        void RegisterUser(RegisterUserDto model);
        string GenerateJwt(LoginDto model);
    }
    public class AccountService : IAccountService
    {
        private readonly DbContextRecipe _context;
        private readonly IMapper _mapper;
        private readonly ILogger<AccountService> _logger;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly AuthenticationSettings _authenticationSettings;

        public AccountService(DbContextRecipe context, IMapper mapper, ILogger<AccountService> logger, IPasswordHasher<User> passwordHasher, AuthenticationSettings authenticationSettings)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
            _passwordHasher = passwordHasher;
           _authenticationSettings = authenticationSettings;
        }

        public string GenerateJwt(LoginDto model)
        {
            var user = _context.Users.Include(r=>r.Role).FirstOrDefault(u => u.Email == model.Email);
            if (user is null)
            {
                throw new BadRequestException("Invalid username or password");
            }

            var result =_passwordHasher.VerifyHashedPassword(user, user.PasswordHash, model.Password);
            if(result == PasswordVerificationResult.Failed)
            {
                throw new BadRequestException("Invalid username or password");
            }

            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Role, $"{user.Role.Name}")
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authenticationSettings.JwtKey));
            var cred = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(_authenticationSettings.JwtExpireDays);

            var token = new JwtSecurityToken(_authenticationSettings.JwtIssuer,
                _authenticationSettings.JwtIssuer,
                claims,
                expires: expires,
                signingCredentials: cred);

            var tokenHandler = new JwtSecurityTokenHandler();
            return tokenHandler.WriteToken(token);
        }

        public List<UserDto> GetAll()
        {
            var users = _context.Users
                .Include(r => r.Role)
                .ToList();

            var result = _mapper.Map<List<UserDto>>(users);

            return result;
        }

        public void RegisterUser(RegisterUserDto model)
        {
            var newUser = _mapper.Map<User>(model);

            string hashedPassword = _passwordHasher.HashPassword(newUser, model.Password);

            newUser.PasswordHash = hashedPassword;
            _context.Users.Add(newUser);
            _context.SaveChanges();
        }
    }
}
