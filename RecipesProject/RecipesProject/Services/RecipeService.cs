﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RecipesMVC.Authorization;
using RecipesMVC.Entities;
using RecipesMVC.Exceptions;
using RecipesMVC.Helpers.Enums;
using RecipesMVC.ModelsDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipesMVC.Services
{
    public interface IRecipeService
    {
        int Create(RecipeDto model);
        void Delete(int id);
        List<RecipeDto> GetAll();
        RecipeDto GetById(int id);
        void Update(int id, RecipeDto model);
    }
    public class RecipeService : IRecipeService
    {
        private readonly DbContextRecipe _context;
        private readonly IMapper _mapper;
        private readonly ILogger<RecipeService> _logger;
        private readonly IAuthorizationService _authorizationService;
        private readonly IUserContextService _userContextService;

        public RecipeService(DbContextRecipe context, IMapper mapper, ILogger<RecipeService> logger, IAuthorizationService authorizationService,
            IUserContextService userContextService)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
            _authorizationService = authorizationService;
            _userContextService = userContextService;
        }
        public int Create(RecipeDto model)
        {
            var recipe = _mapper.Map<Recipe>(model);

            _context.Add(recipe);
            _context.SaveChanges();

            _logger.LogInformation($"CREATE recipe action with id:{recipe.Id}");

            return recipe.Id;
        }

        public void Delete(int id)
        {
            var recipe = _context.Recipes.FirstOrDefault(r => r.Id == id);

            if (recipe is null)
                throw new NotFoundException("Recipe not found");

            var authorization = _authorizationService.AuthorizeAsync(new System.Security.Claims.ClaimsPrincipal(),
                recipe, new ResourceOperationRequirement(ResourceOperation.Delete)).Result;

            if (!authorization.Succeeded)
            {
                throw new ForbiddenException();
            }

            _context.Remove<Recipe>(recipe);
            var result = _context.SaveChanges();

            _logger.LogWarning($"DELETE recipe action for id:{recipe.Id}");
        }

        public List<RecipeDto> GetAll()
        {
            var recipes = _context.Recipes
                .ToList();

            if (recipes is null)
                throw new NotFoundException("Recipes not found");

            var result = _mapper.Map<List<RecipeDto>>(recipes);

            return result;
        }

        public RecipeDto GetById(int id)
        {
            var recipe = _context
                .Recipes
                .Include(c => c.Category)
                .Include(u => u.CreatedByUser)
                .Include(t => t.Tags)
                .FirstOrDefault(r => r.Id == id);

            if (recipe is null)
                throw new NotFoundException("Recipe not found");

            var result = _mapper.Map<RecipeDto>(recipe);

            return result;
        }

        public void Update(int id, RecipeDto model)
        {
            var recipe = _context.Recipes.FirstOrDefault(r => r.Id == id);

            if (recipe is null)
                throw new NotFoundException("Recipe not found");

            recipe = _mapper.Map<Recipe>(model);

            var authorization = _authorizationService.AuthorizeAsync(new System.Security.Claims.ClaimsPrincipal(), 
                recipe, new ResourceOperationRequirement(ResourceOperation.Update)).Result;

            if (!authorization.Succeeded)
            {
                throw new ForbiddenException();
            }

            _context.Update(recipe);
            _context.SaveChanges();

            _logger.LogInformation($"UPDATE recipe action for id:{recipe.Id}");
        }
    }
}
